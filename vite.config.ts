import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path"
import {createSvgIconsPlugin} from "vite-plugin-svg-icons"
import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'
import vueJsx from "@vitejs/plugin-vue-jsx";


export default defineConfig({
    resolve: {
        alias: [{
            find: '@',
            replacement: path.resolve(__dirname, 'src')
        }]
    },
    base: './',
    plugins: [
        vue(),
        vueJsx(),
        Components({
            resolvers: [ElementPlusResolver(), // 自动注册图标组件
                IconsResolver({
                    enabledCollections: ['ep'],
                }),],
        }),
        Icons({
            autoInstall: true,
        }),
        createSvgIconsPlugin({
            iconDirs: [path.resolve(process.cwd(), 'src/assets/svg')],
            symbolId: 'icon-[dir]-[name]'
        }),
    ],
    server: {
        host: '0.0.0.0',
        port: 8090,
        proxy: {
            // 使用 proxy 实例
            '/api': {
              //  target: 'http://127.0.0.1:8248',
                target: 'http://8.137.37.12:8248',
                changeOrigin: true,
                ws: true,
                rewrite: (path: any) => path.replace(/^\/api/, '')
            },
        },
    },
})
