import { createApp } from 'vue'
import pinia from "@/store"
import router from '@/router'
import 'virtual:svg-icons-register'
import * as _lodash from 'lodash'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'nprogress/nprogress.css'
import {$error, $success, $warning} from "@/utils/message";
import * as echarts from 'echarts';
import './style.css'
import App from './App.vue'
import {registerDirective} from "@/directive";



const app = createApp(App)
app.use(router).use(pinia).use(ElementPlus, {
    locale: zhCn,
})
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
registerDirective(app)
app.config.globalProperties.$lodash = _lodash
app.config.globalProperties.$echarts = echarts
app.config.globalProperties.$error = $error
app.config.globalProperties.$success = $success
app.config.globalProperties.$warning = $warning
app.mount('#app')
