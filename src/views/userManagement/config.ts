import {tableLabels} from "@/components/YTable/tableInterface";
import {formatDate} from "@/utils/dayFormDate";

export const formList = [
    {
        inputType: 'input',
        prop: 'nickName',
        label: '昵称',
    },
    {
        inputType: 'select',
        prop: 'registrationType',
        label: '注册类型',
        options: [
            {
                value: '本人',
                label: '本人',
            },
            {
                value: '父母',
                label: '父母'
            },
        ]
    },
    {
        inputType: 'select',
        prop: 'marriage',
        label: '婚姻状况',
        options: [
            {
                value: '已婚',
                label: '已婚',
            },
            {
                value: '未婚',
                label: '未婚'
            },
            {
                value: '离异',
                label: '离异'
            },
        ]
    },
    {
        inputType: 'select',
        prop: 'gender',
        label: '性别',
        options: [
            {
                value: '男',
                label: '男',
            },
            {
                value: '女',
                label: '女'
            }
        ]
    },
    {
        inputType: 'datePicker',
        prop: 'year',
        label: '出生时间',
        type: 'year',
        valueFormat: 'YYYY', //选中后的格式化日期
        format: 'YYYY', //显示的格式
        defaultValue: new Date((new Date().getFullYear() - 18).toString()),
        //禁用今年以后的时间
        disabledDate: (date: Date) => date.getFullYear() > new Date().getFullYear() - 18
    },
    {
        inputType: 'datePicker',
        prop: 'createDate',
        label: '注册时间',
        type: 'daterange',
        startPlaceholder: '开始时间',
        endPlaceholder: '结束时间',
        format: 'YYYY-MM-DD', //显示的格式
        //禁用今年以后的时间
        disabledDate: (date: Date) => {
            return date.getTime() > Date.now();

        }
    },
]

export const searchBtn = [
    {
        label: '搜索',
        name: 'search',
        btnType: 'primary',
        //emit: 'searchEmit', 可以自定义点击事件名字
        isStyle: () => {

        },
    },
    {
        label: '重置',
        btnType: 'warning',
        name: 'reset',
    },
]


export const seeTableColumn: Array<tableLabels> = [
    {
        label: '昵称',
        prop: 'nickName',
        keyId: 1
    },
    {
        label: '注册类型',
        prop: 'registrationType',
        keyId: 2
    },
    {
        label: '年龄',
        prop: 'age',
        keyId: 3
    },
    {
        label: '身高（CM）',
        prop: 'height',
        keyId: 4
    },
    {
        label: '体重（KG）',
        prop: 'weight',
        keyId: 5
    }
]
export const seeTableOptions = {
    label: '操作',
    hidden: () => true,
    children: [
        {
            hidden: true,
            label: '查看',
            methods: 'see',
            btnText: true,
            bg: true,
            size: 'default'
        }
    ]
}
export const tableColumn: Array<tableLabels> = [
    {
        label: '昵称',
        prop: 'nickName',
        keyId: 1
    },
    {
        label: '头像',
        prop: 'headSculpture',
        keyId: 2,
        image: (row: any) => row.headSculpture
    },
    {
        label: '年龄',
        prop: 'age',
        keyId: 3
    },
    {
        label: '身高（CM）',
        prop: 'height',
        keyId: 4
    },
    {
        label: '月收入（元）',
        prop: 'income',
        keyId: 5
    },
    {
        label: '体重（KG）',
        prop: 'weight',
        keyId: 6
    },
    {
        label: '最后登录时间',
        prop: 'lastActiveTime',
        keyId: 7,
        format: (val: number) => formatDate(val)
    },
]
