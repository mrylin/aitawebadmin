import {tableLabels} from "@/components/YTable/tableInterface";
import type {FormInstance, FormRules} from 'element-plus'
import {formatDate} from "@/utils/dayFormDate";

export const formList = [
    {
        inputType: 'input',
        prop: 'nickName',
        label: '昵称',
    },
    {
        inputType: 'select',
        prop: 'registrationType',
        label: '注册类型',
        options: [
            {
                value: '本人',
                label: '本人',
            },
            {
                value: '父母',
                label: '父母'
            },
        ]
    },
    {
        inputType: 'select',
        prop: 'punishmentType',
        label: '处罚情况',
        options: [
            {
                value: '已处罚',
                label: '已处罚',
            },
            {
                value: '未处罚',
                label: '未处罚'
            },
        ]
    },
    {
        inputType: 'select',
        prop: 'gender',
        label: '性别',
        options: [
            {
                value: '男',
                label: '男',
            },
            {
                value: '女',
                label: '女'
            }
        ]
    },
]

export const searchBtn = [
    {
        label: '搜索',
        name: 'search',
        btnType: 'primary',
        //emit: 'searchEmit', 可以自定义点击事件名字
        isStyle: () => {

        },
    },
    {
        label: '重置',
        btnType: 'warning',
        name: 'reset',
    },
]

export const tableColumn: Array<tableLabels> = [
    {
        label: '昵称',
        prop: 'nickName',
        keyId: 1
    },
    {
        label: '注册类型',
        prop: 'registrationType',
        keyId: 2
    },
    {
        label: '年龄',
        prop: 'age',
        keyId: 3
    },
    {
        label: '身高（CM）',
        prop: 'height',
        keyId: 4
    },
    {
        label: '体重（KG）',
        prop: 'weight',
        keyId: 5
    },
    {
        label: '处罚类型',
        prop: 'punishmentType',
        keyId: 6,
        format: (prop: string): string => {
            if (!!prop) {
                return prop
            } else {
                return '---'
            }
        }
    },
    {
        label: '处罚结束时间',
        prop: 'endTime',
        keyId: 7,
        format: (prop: number): string => {
            if (!!prop) {
                return formatDate(prop)
            } else {
                return '---'
            }
        }
    },
]
export const rules: FormRules<any> =
    {
        punishmentType: [
            {
                required: true,
                message: '请选择处罚类型',
                trigger: 'change',
            },
        ],
        endTime: [
            {
                required: true,
                message: '请选择封禁天数',
                trigger: 'change',
            },
        ]
    }

