import {tableLabels} from "@/components/YTable/tableInterface";
import type {FormRules} from 'element-plus'
import {reactive} from "vue/dist/vue";
import {formatDate} from "@/utils/dayFormDate";

const validatePass = (rule: any, value: any, callback: any) => {
    if (value?.length) {
        callback()
    } else {
        callback(new Error('请选择权限'))

    }
}
export const formList = [
    {
        inputType: 'input',
        prop: 'name',
        label: '名称',
    },
]
export const userFormList = [
    {
        inputType: 'input',
        prop: 'name',
        label: '名称',
    }, {
        inputType: 'select',
        prop: 'roleId',
        label: '角色',
        options: [
            {
                value: '1',
                label: '访客',
            },
            {
                value: '2',
                label: '普通管理员'
            },
            {
                value: '3',
                label: '超级管理员'
            },
        ]
    },
]

export const searchBtn = [
    {
        label: '搜索',
        name: 'search',
        btnType: 'primary',
        //emit: 'searchEmit', 可以自定义点击事件名字
        isStyle: () => {

        },
    },
    {
        label: '重置',
        btnType: 'warning',
        name: 'reset',
    },
]

export const tableColumn: Array<tableLabels> = [
    {
        label: '名称',
        prop: 'name',
        keyId: 1
    },
    {
        label: '创建时间',
        prop: 'createTime',
        keyId: 2,
        format: (item: any) => formatDate(item)
    },
]
export const tableUserColumn: Array<tableLabels> = [
    {
        label: '名称',
        prop: 'name',
        keyId: 1
    },
    {
        label: '角色',
        prop: 'roleName',
        keyId: 2
    },
    {
        label: '创建时间',
        prop: 'createTime',
        format: (item: any) => formatDate(item),
        keyId: 3
    },
]
export const rules: FormRules<any> = {
    punishmentType: [
        {
            required: true,
            message: '请选择处罚类型',
            trigger: 'change',
        },
    ],
    days: [
        {
            required: true,
            message: '请选择封禁天数',
            trigger: 'change',
        },
    ]
}
export const addRules: FormRules<any> = {
    name: [
        {
            required: true,
            message: '请输入名称',
            trigger: 'change',
        },
    ],
    roleVal: [
        {
            required: true,
            trigger: 'change',
            validator: validatePass
        },
    ],
}

export const sysTemRules: FormRules<any> = {
    name: [
        {
            required: true,
            message: '请输入名称',
            trigger: 'change',
        },
    ],
    roleVal: [
        {
            required: true,
            message: '请选择角色',
            trigger: 'change',
        },
    ],
}
