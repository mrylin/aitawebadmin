interface config {
    dev: string
    timeOut: number
    formatDate: string
}

export default <config>{
    dev: '',//开发地址
    timeOut: 30000,//axios延迟时间
    formatDate:'YYYY-MM-DD HH:mm'
}
