import {defineStore} from 'pinia'
import {ref} from 'vue'
import {arrToTree} from "@/utils/treeTool";

export const useLoginStore = defineStore('loginStore', () => {
 const loginMenu: any = ref(JSON.parse(<string>localStorage.getItem('loginMenu')) || [])

    const setLoginMenu = (val: any) => {
        loginMenu.value = arrToTree(val);
        localStorage.setItem('loginMenu', JSON.stringify(loginMenu.value))
    }
    return {
        setLoginMenu,
        loginMenu
    }
})
