import {defineStore} from 'pinia'
import {ref} from 'vue'

export const useSearchStore = defineStore('useSearchStore', () => {
    const param:any = ref('' || sessionStorage.getItem('searchObj'))

    const setParam = (val: any): void => {
        param.value = JSON.stringify(val)
        sessionStorage.setItem('searchObj', JSON.stringify(val))
    }

    return {setParam, param}
})
