import {defineStore} from 'pinia'
import {computed, reactive, ref} from 'vue'

interface tagObj {
    titleName?: string
    name?: string
    title?: string
}

interface tags {
    tags: Array<tagObj>
    tagActive: string
}

export const useTagsStore = defineStore('tagsStore', () => {
    const data = reactive<tags>({
        tags: JSON.parse(<string>sessionStorage.getItem('tag')) || [],
        tagActive: '/layouts/home'
    })
    const userInfo: any = ref(JSON.parse(<string>localStorage.getItem("userInfo")) ||{} )

    const getTags = computed(() => data.tags)
    const getCurrentTag = computed(() => data.tagActive)
    const addTags = (item: any) => {
        if (item?.titleName == 'home') {
            data.tags.unshift(item)
        } else {
            data.tags.push(item)
        }
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }
    const addCurrentTag = (item: string) => {
        data.tagActive = item
    }
    const setUserInfo = (item: any) => {
        userInfo.value = item
        localStorage.setItem('userInfo', JSON.stringify(userInfo.value))
    }
    const delTag = (obj: tagObj) => {
        data.tags.splice(data.tags.findIndex((item: tagObj) => item.name === obj.name), 1)
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }
    const resetMenu = () => {
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }
    const emptyMenu = () => {
        sessionStorage.setItem('tag', '')
        data.tags = []
    }


    return {addTags, addCurrentTag, getTags, data, getCurrentTag, delTag, resetMenu,userInfo,setUserInfo,emptyMenu}
})
