import {ElMessageBox} from 'element-plus'
//定义类型只能是这几个名称
type MessageType = '' | 'success' | 'warning' | 'info' | 'error'
import type { Action } from 'element-plus'

export const $msgBox = (msg: string, title: string = '提示', msgType: MessageType = 'warning',
                        confirmButtonText: string = '确定', cancelButtonText: string = '取消',draggable:boolean = true) => {
    return new Promise((resolve, reject) => {
        ElMessageBox.confirm(msg, title, {
            distinguishCancelAndClose: true,
            dangerouslyUseHTMLString: true,
            draggable,
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            type: msgType
        }).then((success) => {
            resolve(success)
        }).catch((action:Action) => {
            if (action == 'cancel') {
                //取消
                reject(action)
            } else {
                reject('close')
            }
        })
    })
}
