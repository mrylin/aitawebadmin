/**
 * 处理多个promise报错
 * @param promise
 * let [result_err, result] = await to(getTree())
 let [people_err, people] = await to(getDataGrid())
 if (result_err || people_err) {
       //处理错误
    } else {
      //回调正常
    }
 }
 */
export function to(promise: Promise<any>) {
    return promise.then((data: any) => [null, data]).catch((err: any) => [err])
}
