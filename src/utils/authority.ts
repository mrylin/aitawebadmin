import {Decrypt, Encrypt} from "@/utils/keyCrypto";
import {flatTree} from "@/utils/treeTool";
import {dataMenu} from "@/layouts/components/menu";
import {useRouter} from "vue-router";
import Router from "@/router";


export function setMenu(val:any): void {
    localStorage.setItem(Encrypt('menu'), Encrypt(JSON.stringify(val)))
}

export const isAuthority = (path: string): boolean => {
    const whitelist = ['/login', '/404', '/403', '/dataAnalysis']
    //不等于登录页和404页的才进入判断是否有权限。等于的话直接放行
    if (!whitelist.includes(path)) {
        const authority = localStorage.getItem(Encrypt('menu'))
        if (authority) {
            const arr = JSON.parse(Decrypt(authority))
            //     console.time('权限')
            const have = arr.filter((item: { path: string; }) => item.path == path)
            //   console.timeEnd('权限')
            //有权限返回false。没有true
            return have.length <= 0;
        }
    }
    return false
}

export const tableButtonAuthority = (permission?: string): boolean => {
    const router = Router
    const menu = localStorage.getItem(Encrypt('menu'))
    const arr = JSON.parse(Decrypt(menu))
    const obj = arr.find((item: any) => item.path == router?.currentRoute?.value?.path)
    return obj?.tableBtnAuthority?.some((eItem: any):boolean => eItem.name == permission);
}
export const tableOperation = () => {
    const router = Router
    const menu = localStorage.getItem(Encrypt('menu'))
    const arr = JSON.parse(Decrypt(menu))
    const obj = arr.find((item: any) => item.path == router?.currentRoute?.value?.path)
    return obj?.tableBtnAuthority?.length
}

export const tableButtonDropdown = (list: any) => {
    const router = Router
    const menu = localStorage.getItem(Encrypt('menu'))
    const arr = JSON.parse(Decrypt(menu))
    let newArr: any = []
    arr.map((item: any) => {
        list.map((it: any) => {
            if (item.path == router?.currentRoute?.value?.path) {
                item?.tableBtnAuthority.map((btnItem: any) => {
                    if (btnItem.name == it.label)
                        newArr.push(it)
                })
            }
        })
    })
    return newArr
}

export function getMenuList(): any {
    const menu = localStorage.getItem(Encrypt('menu'))
    return JSON.parse(Decrypt(menu))
}
