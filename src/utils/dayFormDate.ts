import dayjs from "dayjs";
export const formatDate = (time: number) => dayjs(time).format("YYYY-MM-DD HH:mm")
