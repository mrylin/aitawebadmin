export const routes = [
    {
        path: '/layouts',
        name: 'layouts',
        component: () => import('@/layouts/index.vue'),
        redirect: '/layouts/home',
        children: [
            {
                path: 'home',
                name: 'home',
                meta: {title: '首页'},
                component: () => import('@/views/home.vue'),
            },
            {
                path: 'userManagement',
                name: 'userManagement',
                meta: {title: '用户管理'},
                component: () => import('@/views/userManagement/redirect.vue'),
                children:[
                    {
                        path: 'userInfo',
                        name: 'userInfo',
                        meta: {title: '用户信息'},
                        component: () => import('@/views/userManagement/userInfo.vue'),
                    },
                ]
            },
            {
                path: 'reportingCenter',
                name: 'reportingCenter',
                meta: {title: '举报中心'},
                component: () => import('@/views/reportingCenter/redirect.vue'),
                children:[
                    {
                        path: 'reportingInfo',
                        name: 'reportingInfo',
                        meta: {title: '举报信息'},
                        component: () => import('@/views/reportingCenter/reportingInfo.vue'),
                    },
                ]
            },
            {
                path: 'systemManagement',
                name: 'systemManagement',
                meta: {title: '系统管理'},
                component: () => import('@/views/systemManagement/redirect.vue'),
                children:[
                    {
                        path: 'role',
                        name: 'role',
                        meta: {title: '角色'},
                        component: () => import('@/views/systemManagement/role.vue'),
                    },
                    {
                        path: 'systemUsers',
                        name: 'systemUsers',
                        meta: {title: '系统用户'},
                        component: () => import('@/views/systemManagement/systemUsers.vue'),
                    },
                ]
            },
        ]
    },
    {
        path: '/',
        redirect: '/layouts',
    },
    {
        path: '/login',
        name: 'login',
        meta: {title: '登录'},
        component: () => import('@/views/login.vue'),
    },
    {
        path: '/dataAnalysis',
        name: 'dataAnalysis',
        meta: {title: '大数据分析'},
        component: () => import('@/views/dataAnalysis/index.vue'),
    },
    {
        path: '/404',
        name: '404',
        meta: {title: '404'},
        component: () => import('@/views/error/404.vue'),
    },
    {
        path: "/:catchAll(.*)",
        redirect: '/404'
    }
]
