import {createRouter, createWebHashHistory, createWebHistory, Router} from 'vue-router'
import NProgress from 'nprogress'
import {routes} from './routesArr'
import {blacklist} from "@/router/blacklist";
import {storeToRefs} from "pinia";
import {useTagsStore} from "@/store/useTagsStore";
import pinia from "@/store";
import {isAuthority} from "@/utils/authority";
import {getCookie} from "@/utils/cookieTool";

const store = useTagsStore(pinia)

const {data} = storeToRefs(store);
const {addTags, addCurrentTag} = store

const router: Router = createRouter({
    history: createWebHashHistory(),
    routes
})
router.beforeEach((to, from, next) => {
    NProgress.start()
    // 根据菜单判断用户有没有当前页面权限
    if (isAuthority(to.path)) {
        next({name: '404', replace: true})
    }

    //添加Tag
    let newArr: number //判断当前路径是否在白名单中
    if (!blacklist.includes(to.name)) {
        addCurrentTag(to.path)
        newArr = data.value?.tags?.findIndex(v => v.name == to.path);
        if (newArr == -1) addTags({titleName: to.name, name: to.path, title: to.meta.title})
    }
    document.title = to.meta?.title as any
    //判断cookie是否存在
    to.name == 'login' ? next() : getCookie('token') ? next() : next({name: 'login', replace: true})
})
router.afterEach((to, from) => {
    NProgress.done()
})


export default router
