export const dataMenu = [{
    name: '菜单树',
    id: 0,
    pid: null,
    children: [
        {
            icon: 'shouye',
            name: '首页',
            path: '/layouts/home',
            id: 1,
            pid: 0,
            children: []
        },
        {
            icon: 'shujufenxi',
            name: '数据分析',
            path: '/dataAnalysis',
            id: 2,
            pid: 0,
            children: []
        },
        {
            icon: 'guanliyuan',
            name: '用户管理',
            path: '',
            id: 3,
            pid: 0,
            children: [
                {
                    icon: 'yonghuguanli',
                    name: '用户信息',
                    path: '/layouts/userManagement/userInfo',
                    id: 4,
                    pid: 3,
                    tableBtnAuthority: [
                        {id: 1, name: '查看'},
                        {id: 2, name: '聊天记录'},
                        {id: 3, name: '删除'},
                    ],
                    children: []
                }
            ]
        },
        {
            icon: 'jubao',
            name: '举报中心',
            path: '',
            id: 7,
            pid: 0,
            children: [
                {
                    icon: 'jubaoxinxichuli',
                    name: '举报信息',
                    path: '/layouts/reportingCenter/reportingInfo',
                    id: 8,
                    pid: 7,
                    tableBtnAuthority: [
                        {id: 1, name: '查看'},
                        {id: 2, name: '处罚'},
                        {id: 3, name: '删除'},
                    ],
                    children: []
                },
            ]
        },
        {
            icon: 'xitongguanli',
            name: '系统管理',
            path: '',
            id: 10,
            pid: 0,
            children: [
                {
                    icon: 'jiaoseguanli',
                    name: '角色',
                    path: '/layouts/systemManagement/role',
                    id: 11,
                    pid: 10,
                    //页面按钮权限
                    buttonList: [
                        {id: 1, name: '新增'}
                    ],
                    //表格按钮权限
                    tableBtnAuthority: [
                        {
                            id: 1,
                            name: '权限'
                        },
                        {
                            id: 2,
                            name: '删除'
                        },
                    ],
                    children: []
                },
                {
                    icon: 'xitongyonghu',
                    name: '系统用户',
                    path: '/layouts/systemManagement/systemUsers',
                    id: 12,
                    pid: 10,
                    //页面按钮权限
                    buttonList: [
                        {id: 1, name: '新增'}
                    ],
                    //表格按钮权限
                    tableBtnAuthority: [
                        {
                            id: 1,
                            name: '重置密码'
                        },
                        {
                            id: 2,
                            name: '权限'
                        },
                        {
                            id: 3,
                            name: '删除'
                        },
                    ],
                    children: []
                },
            ]
        },
    ]
}]
