import {permission} from "./permission";

export const registerDirective = (app: any) => {
    app.directive('permission', permission)
}
