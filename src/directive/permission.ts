import {Decrypt, Encrypt} from "@/utils/keyCrypto";
import Router from "@/router";

export const permission = {
    mounted: (el: any, binding: any) => {
        const router = Router
        const menu = localStorage.getItem(Encrypt('menu'))
        const arr = JSON.parse(Decrypt(menu))
        arr.map((item: any) => {
            if (item.path == router.currentRoute.value.path) {
                if (!(item?.buttonList?.some((btnItem: any) => btnItem.name == binding.value))) {
                    el?.remove?.()
                }
            }
        })
    }
}
