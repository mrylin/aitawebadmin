import {get, post, downloadFileProgress} from './index'

export const getPageAdminUser = (data: any): any => get('/adminUser/getPageAdminUser', data)
export const getRole = (): any => get('/role/getRole')
export const updateAdminUser = (data:any): any => post('/adminUser/updateAdminUser',data)
export const addAdminUser = (data:any): any => post('/adminUser/addAdminUser',data)
export const updatePassword = (data:any): any => post('/adminUser/updatePassword',data)
export const delAdminUser = (data:any): any => post('/adminUser/delAdminUser',data)
