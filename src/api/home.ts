import {get, post, downloadFileProgress} from './index'

export const activeUser = (): any => get('/user/activeUser')
export const signUpToday = (): any => get('/user/signUpToday')
export const signCount = (): any => get('/user/signCount')
export const todayActiveUsers = (data:any): any => get('/user/todayActiveUsers',data)
export const signUser = (data:any): any => get('/user/signUser',data)
export const updatePass = (data:any): any => post('/adminUser/updatePass',data)
