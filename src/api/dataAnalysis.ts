import {get, post, downloadFileProgress} from './index'

export const activeUsers = (): any => get('/user/activeUsers')
export const complaintsPenalties = (): any => get('/user/complaintsPenalties')
export const activeMap = (): any => get('/user/activeMap')
export const activityRate = (): any => get('/user/activityRate')
export const registrantsUser = (): any => get('/user/registrantsUser')
