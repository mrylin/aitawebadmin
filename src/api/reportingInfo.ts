import {get, post, downloadFileProgress} from './index'

export const getReport = (data: any): any => get('/report/getReport', data)
export const getMsgList = (data: any): any => get('/report/getMsgList', data)
export const updatePunish = (data: any): any => post('/report/updatePunish', data)
export const delPunish = (data: any): any => post('/report/delPunish', data)
