import {get, post, downloadFileProgress} from './index'

export const getUserList = (data: any): any => get('/user/getUserList', data)
export const getMsgList = (data: any): any => get('/user/getMsgList', data)
export const getMsgDetailed = (data: any): any => get('/user/getMsgDetailed', data)
