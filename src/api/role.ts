import {get, post, downloadFileProgress} from './index'

export const getPageRole = (data: any): any => get('/role/getPageRole', data)
export const getAuthorityMenu = (): any => get('/role/getAllMenu')
export const addRoleItem = (data: any): any => post('/role/addRole',data)
export const updateRole = (data: any): any => post('/role/updateRole',data)
export const delRole = (data: any): any => post('/role/delRole',data)
